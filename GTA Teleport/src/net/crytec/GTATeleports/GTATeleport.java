package net.crytec.GTATeleports;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.GTATeleports.language.LanguageUtil;
import net.crytec.GTATeleports.metrics.Metrics;

public class GTATeleport extends JavaPlugin {
	
	private static GTATeleport instance;
	private TeleportManager tpmgr;
	
	@Override
	public void onLoad() {
		GTATeleport.instance = this;
	}
	
	
	@Override
	public void onEnable() {
		long start = System.currentTimeMillis();
		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}

		File config = new File(getDataFolder(), "config.yml");
		new LanguageUtil();

		try {
			if (!config.exists()) {
				config.createNewFile();
				saveResource("config.yml", true);
				getLogger().info("Setup - New default configuration has been written.");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		Bukkit.getPluginManager().registerEvents(new Listeners(this), this);
		tpmgr = new TeleportManager(this);
		getCommand("gtatp").setExecutor(new GTACommand());
		getLogger().info("Enabled in " + (System.currentTimeMillis() - start) + " ms." );
		
		updateConfig("UseMetrics", true);
		
		if (getConfig().getBoolean("UseMetrics")) {
		    try {
		        Metrics metrics = new Metrics(this);
		        metrics.start();
		    } catch (IOException e) {
		    	getLogger().severe("Unable to initialize Metrics.");
		    }
		}
		
	}
	
	
	public static GTATeleport getInstance() {
		return GTATeleport.instance;
	}
	
	public TeleportManager getTeleportManager() {
		return this.tpmgr;
	}
	
	@Override
	public void reloadConfig() {
		super.reloadConfig();
		new LanguageUtil();
	}
	
	public void updateConfig(String path, Object value) {
		if (!getConfig().isSet(path)) {
			getConfig().set(path, value);
			saveConfig();
		}
	}

}
