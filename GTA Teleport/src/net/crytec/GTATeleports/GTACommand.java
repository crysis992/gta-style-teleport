package net.crytec.GTATeleports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.crytec.GTATeleports.language.Language;
import net.md_5.bungee.api.ChatColor;

public class GTACommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
			if (!sender.hasPermission("gtatp.reload")) {
				sender.sendMessage(ChatColor.RED + "You lack the proper permissions to use this command.");
				return true;
			}
			GTATeleport.getInstance().reloadConfig();
			sender.sendMessage(Language.CMD_RELOAD.toChatString());
			return true;
		}
		return false;
	}
}