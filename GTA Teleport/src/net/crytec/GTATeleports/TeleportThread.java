package net.crytec.GTATeleports;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class TeleportThread {

	
	private Player player;
	private Location destination;
	private Location from;
	private GTATeleport plugin;
	private BukkitTask thread;
	
	private int dropsUp = 3;
	private int targetY = 150;
	
	private float pitch;
	private float yaw;
	
	private Sound sound = Sound.ENTITY_ENDERMEN_TELEPORT;
	
	private GameMode gmfrom;
	private float speed;
	
	public TeleportThread(GTATeleport plugin, Player player, Location from,  Location destination) {
		this.player = player;
		this.plugin = plugin;
		this.from = from;
		this.destination = destination;
		
		this.pitch = player.getLocation().getPitch();
		this.yaw = player.getLocation().getYaw();
		gmfrom = player.getGameMode();
		speed = player.getFlySpeed();
		dropsUp = plugin.getConfig().getInt("dropsUp");
		targetY = plugin.getConfig().getInt("targetY");
		
		Sound s = Sound.valueOf(plugin.getConfig().getString("sound").toUpperCase());
		
		if (s != null) {
			this.sound = s;
		}
		
		moveUp();
	}
	
	
	public void moveUp() {
		
		player.setGameMode(GameMode.SPECTATOR);
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.hidePlayer(player);
		}
		
		player.setFlySpeed(0F);
		
		
		Chunk chunk = destination.getWorld().getChunkAt(destination);
		
		if (!chunk.isLoaded()) chunk.load();
		
		thread = new BukkitRunnable() {
			
			int countUp = 0;
			
			@Override
			public void run() {
				
				if (countUp == dropsUp) {
					cancel();
					Location dest = destination.clone();
					dest.setY(targetY);
					dest.setPitch(90.0F);
					dest.setYaw(0.0F);
					player.teleport(dest);
					moveDown();
					return;
				}
				
				Location loc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() + (targetY - from.getY()) / dropsUp, player.getLocation().getZ());
				loc.setPitch(90.0F);
	            loc.setYaw(0.0F);
	            
	            player.teleport(loc);
	            player.playSound(player.getLocation(), sound, 1, 1);
				countUp++;
			}
		}.runTaskTimer(plugin, 1L, (plugin.getConfig().getInt("dropdelay") * 20));
	}
	
	public void moveDown() {
		thread = new BukkitRunnable() {
			
			int countDown = 0;
			
			@Override
			public void run() {
				
				if (countDown == dropsUp) {
					cancel();
					end(false);
					return;
				}
				
				Location loc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() + (targetY - from.getY()) / dropsUp, player.getLocation().getZ());
				loc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() - (targetY - (destination != null ? destination.getY() : from.getY())) / dropsUp, player.getLocation().getZ());
				loc.setPitch(90.0F);
	            loc.setYaw(0.0F);
	            player.teleport(loc);
	            player.playSound(player.getLocation(), sound, 1, 1);
	            countDown++;
			}
		}.runTaskTimer(plugin, (plugin.getConfig().getInt("downdelay") * 20), (plugin.getConfig().getInt("dropdelay") * 20));
	}
	
	public void end(boolean force) {
		player.setFlySpeed(speed);
		player.setGameMode(gmfrom);
		
		Location loc = player.getLocation().clone();
		loc.setPitch(pitch);
		loc.setYaw(yaw);
		player.teleport(loc, TeleportCause.PLUGIN);
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.showPlayer(player);
		}
		
		if (force) {
			player.teleport(destination);
			player.setFallDistance(0F);
			thread.cancel();
		} else {
			plugin.getTeleportManager().endTeleport(player, false);
		}
	}
}
