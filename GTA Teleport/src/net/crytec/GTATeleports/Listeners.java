package net.crytec.GTATeleports;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class Listeners implements Listener {
	
	private GTATeleport plugin;
	
	private HashSet<UUID> exception = new HashSet<UUID>();
	private HashSet<String> ignore_cmds = new HashSet<String>();
	
	public Listeners(GTATeleport instance) {
		this.plugin = instance;
		ignore_cmds.add("ascend");
		ignore_cmds.add("descend");
	}
	

	
	@EventHandler
	public void onJoin(PlayerLoginEvent e) {
		exception.add(e.getPlayer().getUniqueId());
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		if (!plugin.getConfig().getStringList("enabled_worlds").contains(e.getFrom().getWorld().getName())) {
			return;
		} else {
			if (!e.getPlayer().hasPermission("gtatp.use")) { return; }
			if (plugin.getTeleportManager().isTeleporting(e.getPlayer())) return;
			if (exception.contains(e.getPlayer().getUniqueId())) {
				// Teleport after Login
				exception.remove(e.getPlayer().getUniqueId());
				return;
			}
			plugin.getTeleportManager().startTeleport(e.getPlayer(), e.getFrom(), e.getTo());
			e.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void exceptionCommands(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().contains("ascend") ) {
			exception.add(e.getPlayer().getUniqueId());
		}
	}
	
	@EventHandler
	public void playerInteractEvent(PlayerInteractEvent e) {
		if (e.getItem() != null && e.getItem().getType() == Material.COMPASS) {
			if (e.getPlayer().hasPermission("worldedit.navigation.jumpto")) {
				exception.add(e.getPlayer().getUniqueId());
			}
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if (!plugin.getTeleportManager().isTeleporting(e.getPlayer())) return;
		
		Location loc = e.getFrom().clone();
		loc.setPitch(90.0F);
        loc.setYaw(0.0F);
		
		e.setTo(loc);
		
	}
	

}
