package net.crytec.GTATeleports.language;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
 
/**
* An enum for requesting strings from the language file.
*/

public enum Language {
    TITLE("title", "&2[&fGTATeleport&2]"),
    CMD_RELOAD("command.reload", "&aConfiguration has been reloaded!");
	
    private String path;
    private String def;
    private static YamlConfiguration LANG;
 
    /**
    * Lang enum constructor.
    * @param path The string path.
    * @param start The default string.
    */
    Language(String path, String start) {
        this.path = path;
        this.def = start;
    }
 
    /**
    * Set the {@code YamlConfiguration} to use.
    * @param config The config to set.
    */
    public static void setFile(YamlConfiguration config) {
        LANG = config;
    }
    
    public static YamlConfiguration getFile() {
    	return LANG;
    }
 
    @Override
    public String toString() {
        if (this == TITLE) return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def)) + " ";
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }
    
    /**
     * Get the String with the TITLE
     * @return
     */
    public String toChatString() {
    	return TITLE.toString() + ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }
     
    /**
    * Get the default value of the path.
    * @return The default value of the path.
    */
    public String getDefault() {
        return this.def;
    }
 
    /**
    * Get the path to the string.
    * @return The path to the string.
    */
    public String getPath() {
        return this.path;
    }
}
