package net.crytec.GTATeleports.language;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import net.crytec.GTATeleports.GTATeleport;

public class LanguageUtil {
	
	
	public LanguageUtil() {
		loadLang();
		update();
	}
	
	
	private void update() {
		File lang = new File(GTATeleport.getInstance().getDataFolder(), "lang.yml");
		
		if (!Language.getFile().isSet("interface.manage.info.description")) Language.getFile().set("interface.manage.info.description", 
//				Arrays.asList("&6Heigth: %heigth% Blocks", "&6Depth: %depth% Blocks", "&6Size: %size% x %size%", "&cRefund: %refund% Dollar"));
		Arrays.asList("&6>>&7This is the default configuration", "&6>>&7 You can define a custom text which", "&6>>&7is shown in this menu :)"));
		
		try {
			Language.getFile().save(lang);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadLang() {
		File lang = new File(GTATeleport.getInstance().getDataFolder(), "lang.yml");
		if (!lang.exists()) {
			try {
				GTATeleport.getInstance().getDataFolder().mkdir();
				lang.createNewFile();
				if (lang != null) {
					YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(lang);
					defConfig.save(lang);
					Language.setFile(defConfig);
				}
			} catch (IOException e) {
				GTATeleport.getInstance().getLogger().severe("�cCouldn't create language file.");
				Bukkit.getPluginManager().disablePlugin(GTATeleport.getInstance());
			}
		}
		
		YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
		for (Language item : Language.values()) {
			if (conf.getString(item.getPath()) == null) {
				conf.set(item.getPath(), item.getDefault());
			}
		}
		Language.setFile(conf);
		try {
			conf.save(lang);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}