package net.crytec.GTATeleports;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportManager {

	private GTATeleport plugin;
	private HashMap<UUID, TeleportThread> teleporting = new HashMap<UUID, TeleportThread>();
	
	
	public TeleportManager(GTATeleport instance) {
		this.plugin = instance;
	}
	
	
	public void startTeleport(Player player, Location destination, Location from) {
		teleporting.put(player.getUniqueId(), new TeleportThread(plugin, player, destination, from));
	}
	
	public boolean isTeleporting(Player player) {
		return teleporting.containsKey(player.getUniqueId());
	}
	
	public void endTeleport(Player player, boolean force) {
		if (force) {
			teleporting.get(player.getUniqueId()).end(true);
		}
		teleporting.remove(player.getUniqueId());
	}
}